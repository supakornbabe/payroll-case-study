
package payrollcasestudy.entities.paymentmethods;

import payrollcasestudy.entities.PayCheck;

/**
 * represents the payment method which is held by paymentmaster for pick up
 * Created by Supakorn Wongsawang 5910500147 & Nanthawan Hanvinyanan 5910505343
 */
public class HoldMethod implements PaymentMethod
{
    @Override
    public void pay(PayCheck payCheck) 
    {
        payCheck.setField("Disposition", "Hold");
    }
}