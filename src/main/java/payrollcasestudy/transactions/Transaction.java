
package payrollcasestudy.transactions;

/**
 * base class for all types of transactions
 * Created by Supakorn Wongsawang 5910500147 & Nanthawan Hanvinyanan 5910505343
 */
public interface Transaction
{

	public void execute( );
}
