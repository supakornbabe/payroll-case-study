
package payrollcasestudy.transactions.change;

import payrollcasestudy.entities.Employee;

/**
 * Created by Supakorn Wongsawang 5910500147 & Nanthawan Hanvinyanan 5910505343
 */
public class ChangeAddressTransaction extends ChangeEmployeeTransaction
{

	private String newAddress;

	public ChangeAddressTransaction( int employeeId, String newAddress )
	{
		super( employeeId );
		this.newAddress = newAddress;
	}

	@Override
	public void changeEmployee( Employee employee )
	{
		employee.setAddress( this.newAddress );
	}
}
