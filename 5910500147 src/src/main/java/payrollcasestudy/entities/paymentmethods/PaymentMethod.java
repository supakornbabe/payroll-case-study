
package payrollcasestudy.entities.paymentmethods;

import payrollcasestudy.entities.PayCheck;

/**
 * PaymentMethod is specified by fields inside payCheck
 * Created by Supakorn Wongsawang 5910500147 & Nanthawan Hanvinyanan 5910505343
 */
public interface PaymentMethod
{
    void pay(PayCheck payCheck);
}
